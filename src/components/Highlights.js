import {Figure} from 'react-bootstrap'

export default function Home() {
	return(
		<div>
			<div className="image-grid">
				<img className="image-grid-col-img1 image-grid-row-img1" width="800" src="/HeroPage/SON.png" alt=""/>
				<img className="image-grid-col-img2 image-grid-row-img2" src="/HeroPage/Steel.png" alt=""/>
				<img className="image-grid-col-img3 image-grid-row-img3" src="/HeroPage/Tech.png" alt=""/>
			</div>
			<div>
				<Figure>
					<Figure.Image src="/HeroPage/Banner.png"/>
				</Figure>
			</div>
		</div>
	);
};