import {useState, useEffect} from 'react'
import {Row, Col, Card, Container, Image} from 'react-bootstrap'
import {useParams} from 'react-router-dom';

export default function ProductView() {
	const [productInfo, setProductInfo] = useState({
		name: null,
		price: null
	});

	const {productId} = useParams()
	useEffect(() => {
		fetch(`https://capstone3-mdnoxdeus.herokuapp.com/user/${productId}`)
		.then(res => res.json())
		.then(convertedData => {
			setProductInfo({
				name: convertedData.name,
				price: convertedData.price,
				image: convertedData.image
			});
		});
	},[productId]);

	return(
		<div>	
			<Row>
				<Col>
					<Container>
						<Card className="text-center p-3 mt-5">
						<Card.Body>
							<Image src={productInfo.image} alt=""/>		
							<Card.Title>
								<h3>{productInfo.name}</h3>
							</Card.Title>
							<Card.Subtitle>
								<h6 className="my-4">Price: </h6>
							</Card.Subtitle>
							<Card.Text>
								<h5>{productInfo.price}</h5>
							</Card.Text>
						</Card.Body>
						</Card>
					</Container>
				</Col>
			</Row>
		</div>
	);
};