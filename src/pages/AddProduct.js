import {useState, useEffect} from 'react'
import {Container, Form, Button} from 'react-bootstrap';
import Banner from	'./../components/Banner';
import Swal from 'sweetalert2'

const details = {
	title: 'NEW PRODUCT',
}

export default function AddProduct() {
	let token = localStorage.getItem('accessToken');

	const [productName, setProductName] = useState('');
	const [productPrice, setProductPrice] = useState('');
	const [productImage, setProductImage] = useState(''); 

	const [isAdded, setIsAdded] = useState(false);

	useEffect(() => {
		if (productName !== '' && productPrice !== '' && productImage !== '') {
			setIsAdded(true);
		} else {
			setIsAdded(false);
		};
	},[productName, productPrice, productImage]);

	const createProduct = async(eventSubmit) => {
		eventSubmit.preventDefault()

		const isAdded = await fetch('https://capstone3-mdnoxdeus.herokuapp.com/admin/newproduct', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				name: productName,
				price: productPrice,
				image: productImage
			})
		}).then(res => res.json()).then(jsondata => {
			if (jsondata) {
				return true;
			} else {
				return false;
			}
		})

		if (isAdded) {
			setProductName('');
			setProductPrice('');
			setProductImage('');
			await Swal.fire({
				icon: 'success',
				title: 'New Product Created',
			})
			window.location.href = "/admin";
		} else {
			Swal.fire({
				icon: 'error',
				title: 'Something went wrong.',
				text: 'Try again later.'
			});
		};
	};

	return(
		<>
			<Banner className="text-center" bannerData={details}/>
			<Container className="mb-5">
				<Form onSubmit={e => createProduct(e)}>
					<Form.Group>
						<Form.Label>Product Name:</Form.Label>
						<Form.Control 
							type="text" 
							required
							value={productName}
							onChange={e => setProductName(e.target.value)}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Product Price:</Form.Label>
						<Form.Control 
							type="number"
							required
							value={productPrice}
							onChange={e => setProductPrice(e.target.value)}
						/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Product Image:</Form.Label>
						<Form.Control 
							type="text"
							required
							value={productImage}
							onChange={e => setProductImage(e.target.value)}
						/>
					</Form.Group>
					
						{
							isAdded ? <Button className="btn-secondary btn-block" type="submit">Add Product</Button>
							: <Button className="btn-secondary btn-block" disabled>Add Product</Button>
						}
				</Form>
			</Container>
		</>
	);
};
