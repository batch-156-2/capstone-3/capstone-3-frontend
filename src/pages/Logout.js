import {useContext, useEffect} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

export default function Logout() {
	const {setUser, unsetUser} = useContext(UserContext);
	unsetUser();
	useEffect(() => {
		setUser({
			id: null,
			isAdmin: null
		})
		Swal.fire({
			icon: 'success',
			title: 'Logged out successfully'
		})
	},[setUser]);

	return(
		<Navigate to="/" replace={true}/>
	);
};