import {Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import Banner from './../components/Banner';

const data = {
	title: 'ADMIN CONTROLS',
};

export default function Admin() {
	return(
		<div>
			<Banner bannerData={data}/>
			<Container>
				<Link className="btn btn-secondary btn-block" to="/admin/addproduct">
					Create New Product
				</Link>
				<Link className="btn btn-secondary btn-block" to="/admin/allproducts">
					All Products
				</Link>
			</Container>
		</div>
	)
};