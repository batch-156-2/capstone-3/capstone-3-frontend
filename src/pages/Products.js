import {useState, useEffect} from 'react'
import Hero from './../components/Banner';
import ProductCard from './../components/ProductCard';
import {Container} from 'react-bootstrap'

const bannerDetails = {
	title: 'SHOP'
}

export default function Products() {
	const [products, setProducts] = useState([]);
	useEffect(() => {
		fetch('https://capstone3-mdnoxdeus.herokuapp.com/user/active')
		.then(res => res.json())
		.then(convertedData => {
			setProducts(convertedData.map(product => {
				return(<ProductCard key={product._id} productProp={product}/>)
			}))
		});
	},[]);

	return(
		<div className="text-center">
			<Hero bannerData={bannerDetails}/>
			<Container>
				{products}
			</Container>
		</div>
	)
}