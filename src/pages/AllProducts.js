import {useState, useEffect} from 'react'
import Hero from './../components/Banner';
import UpdateCard from './../components/UpdateCard';
import {Container} from 'react-bootstrap'

const bannerDetails = {
	title: 'ALL PRODUCTS'
}

export default function AllProducts() {
	let token = localStorage.getItem('accessToken');
	const [allProducts, setAllProducts] = useState([]);
	useEffect(() => {
		fetch('https://capstone3-mdnoxdeus.herokuapp.com/admin/all', {
			headers: {
				'Authorization': `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(convertedData => {
			setAllProducts(convertedData.map(product => {
				return(<UpdateCard key={product._id} updateProp={product}/>)
			}))
		});
	});

	return(
		<div className="text-center">
			<Hero bannerData={bannerDetails}/>
			<Container>
				{allProducts}
			</Container>
		</div>
	)
}